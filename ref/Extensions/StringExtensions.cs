﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Extensions.Algorithmen;

namespace Extensions
{
    public static class StringExtensions
    {
        public static string Remove(this string st, params string[] remove)
        {
            return remove.Aggregate(st, (current, s) => current.Replace(s, ""));
        }

        public static string[] Split(this string testString, string splitter)
        {
            return testString.Split(new string[] { splitter }, StringSplitOptions.None);
        }

        public static int WordCount(this String str)
        {
            return str.Split(new char[] { ' ', '.', '?', '!' }, StringSplitOptions.RemoveEmptyEntries).Length;
        }

        public static string[] Split(this String testString, string split, StringSplitOptions SSO)
        {
            return testString.Split(new string[] { split }, SSO);
        }

        public static long GrößeToLong(this String str)
        {
            string[] ext = {"Byte", "KB", "MB", "GB", "TB"};
            string[] split = str.Split(' ');
            long l;
            long.TryParse(split[0].Split(',')[0].Replace(".", ""), out l);
            if (ext.Contains(split[1]))
            {
                int index = ext.ToList().IndexOf(split[1]);
                l *= (long) Math.Pow(1024, index);
            }
            return l;
        }

        public static uint ToUint16(this String testString)
        {
            return Convert.ToUInt16(testString);
        }

        public static int ToInt32(this String testString)
        {
            return Convert.ToInt32(testString);
        }

        public static double ToDouble(this String testString)
        {
            return Convert.ToDouble(testString);
        }

        public static long ToInt64(this String testString)
        {
            return Convert.ToInt64(testString);
        }

        public static string HtmlUrlDecode(this String htmlurl)
        {
            htmlurl = htmlurl.Replace("\\/", "/");
            htmlurl = HttpUtility.HtmlDecode(htmlurl);
            return HttpUtility.UrlDecode(htmlurl);
        }

        public static string HtmlDecode(this String html)
        {
            html = html.Replace("\\/", "/");
            return HttpUtility.HtmlDecode(html);
        }

        public static string HtmlEncode(this String text)
        {
            return HttpUtility.HtmlEncode(text);
        }

        public static string UrlDecode(this String url)
        {
            url = url.Replace("\\/", "/");
            return HttpUtility.UrlDecode(url);
        }
        [Obsolete("Use HtmlDecode instead")]
        public static string HTMLToString(this String htmlString)
        {
            htmlString = htmlString.Replace("&uuml;", "ü").Replace("&Uuml;", "Ü");
            htmlString = htmlString.Replace("&ouml;", "ö").Replace("&Ouml;", "Ö");
            htmlString = htmlString.Replace("&auml;", "ä").Replace("&Auml;", "Ä");
            htmlString = htmlString.Replace("&szlig;", "ß").Replace("&quot;", "\"");
            htmlString = htmlString.Replace("&#039;", "'").Replace("&amp;", "&");
            htmlString = htmlString.Replace("&#8211;", "–").Replace("%20", " ");
            htmlString = htmlString.Replace("%21", "!").Replace("%3A", ":");
            htmlString = htmlString.Replace("%C3%BC", "ü").Replace("%C3%A4", "ä");
            htmlString = htmlString.Replace("%C3%B6", "ö").Replace("%0D%0A", "\r\n");
            htmlString = htmlString.Replace("%3C", "<").Replace("%2F", "/");
            htmlString = htmlString.Replace("%3D", "=").Replace("%22", "\"");
            htmlString = htmlString.Replace("%3E", ">").Replace("&bdquo;", "");
            return htmlString;
        }

        public static string Reverse(this String s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        public static string StartBig(this String s)
        {
            s = s.ToLower();
            string[] words = s.Split(' ');
            s = "";
            foreach (string w in words)
            {
                try
                {
                    s += w[0].ToString().ToUpper() + w.Substring(1) + " ";
                }
                catch { }
            }
            s = s.TrimEnd(' ');
            return s;
        }

        public static string Base64Decode(this String s)
        {
            return System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(s));
        }

        public static bool IsNullOrEmpty(this String s)
        {
            return string.IsNullOrEmpty(s);
        }

        public static string ToPath(this String s)
        {
            string invalid = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());

            return invalid.Aggregate(s, (current, c) => current.Replace(c.ToString(), ""));
        }

        public static string Substring(this String s, int startindex, long endindex)
        {
            try
            {
                s = s.Remove(endindex.ToInt32());
            }
            catch
            {
            }
            return s.Remove(0, startindex);
        }

        public static string Substring(this String s, string startstring, string endstring)
        {
            s = s.Remove(0, s.IndexOf(startstring) + startstring.Length);
            return s.Remove(s.IndexOf(endstring));
        }

        public static string Substring(this String s, string startstring, string endstring, bool include)
        {
            if (!include)
            {
                return s.Substring(startstring, endstring);
            }
            else
            {
                s = s.Remove(0, s.IndexOf(startstring));
                return s.Remove(s.IndexOf(endstring)+endstring.Length);
            }
        }

        public static string RemoveHtmlStrings(this String s)
        {
            List<string> tags = new List<string> {"<a", "</", "<div", "<img", "<strong", "<h", "<span","<p","<li", "<ul"};
            foreach (string tag in tags)
            {
                try
                {
                    while (true)
                    {
                        string rem = s.Substring(tag, ">", true);
                        s = s.Replace(rem, "");
                    }
                }
                catch
                {
                }
            }
            return s;
        }

        public static List<Diff> Differences(this String s1, String s2)
        {
            diff_match_patch dmp = new diff_match_patch();
            return dmp.diff_main(s1, s2);
        }

        public static bool ContainsAny(this String s, params string[] strings)
        {
            return strings.Any(s.Contains);
        }

        public static bool ContainsAll(this String s, params string[] strings)
        {
            return strings.All(s.Contains);
        }

        public static List<string> Substrings(this String s, string startswith, string endswith)
        {
            List<int> starts = new List<int>();
            List<int> ends = new List<int>();
            int i = 0;
            while ((i = s.IndexOf(startswith, i)) >= 0)
            {
                starts.Add(i);
                i += startswith.Length;
            }
            i = 0;
            while ((i = s.IndexOf(endswith, i)) >= 0)
            {
                ends.Add(i);
                i += endswith.Length;
            }
            //int index = 0;
            //while ((index = s.IndexOf(startswith, index))>=0)
            //{
            //    int end = s.IndexOf(endswith, index);
            //    if (end < 0)
            //        break;
            //    list.Add(s.Substring(index, (long)(end + endswith.Length)));
            //    index = end + endswith.Length;
            //}
            return (from end in ends let max = starts.Where(d => d < end).Max() select s.Substring(max, end - max + endswith.Length)).ToList();
        }

        public static byte[] HexToByteArray(this String hex)
        {
            return Enumerable.Range(0, hex.Length)
                .Where(x => x % 2 == 0)
                .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                .ToArray();
        }

        public static int HexToDec(this String hexValue)
        {
            return Int32.Parse(hexValue, System.Globalization.NumberStyles.HexNumber);
        }

        public static bool IsInt32(this String s)
        {
            try
            {
                s.ToInt32();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}