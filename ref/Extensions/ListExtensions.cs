﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extensions
{
    public static class ListExtensions
    {
        public static List<T> Sublist<T>(this List<T> list, int start, int end)
        {
            List<T> sub = new List<T>();
            for (int i = start; i <= end && i<list.Count; i++)
            {
                sub.Add(list[i]);
            }
            return sub;
        }

        public static bool Contains<T>(this List<T> list, Func<T, bool> predicate)
        {
            return list.FirstOrDefault(predicate) != null;
        }

        public static void AddIfNotInside<T>(this List<T> list, IEnumerable<T> items)
        {
            foreach (T item in items)
            {
                list.AddIfNotInside(item);
            }
        }

        public static void AddIfNotInside<T>(this List<T> list, T item)
        {
            if (!list.Contains(item))
                list.Add(item);
        }

        public static T FirstOrNew<T>(this List<T> list, Func<T, bool> predicate) where T : new()
        {
            T item = list.FirstOrDefault(predicate);
            if (item == null)
            {
                item = new T();
                list.Add(item);
            }
            return item;
        }
    }
}
