﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extensions
{
    public static class IntExtensions
    {
        public static string SekudenZuZeitString(this int sekunden)
        {
            int min = sekunden / 60;
            int h = min / 60;
            if (h > 0)
            {
                min %= 60;
                sekunden %= 60;
                return string.Format("{0}h {1}m {2}s", h, min, sekunden);
            }
            else if (min > 0)
            {
                sekunden %= 60;
                return string.Format("{0}m {1}s", min, sekunden);
            }
            return string.Format("{0}s", sekunden);
        }
    }
}
