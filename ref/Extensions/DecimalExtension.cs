﻿using System;

namespace Extensions
{
    public static class DecimalExtension
    {
        public static int ToInt32(this decimal d)
        {
            return Convert.ToInt32(d);
        }
    }
}