﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.ComponentModel;
using System.Xml.Serialization;
using System.IO;
//using Polenter.Serialization;

namespace Extensions
{
    public static class SynchronizeExtensions
    {
        public static TResult SafeInvoke<T, TResult>(this T isi, Func<T, TResult> call) where T : ISynchronizeInvoke
        {
            if (isi.InvokeRequired)
            {
                IAsyncResult result = isi.BeginInvoke(call, new object[] { isi });
                object endResult = isi.EndInvoke(result); return (TResult)endResult;
            }
            else
                return call(isi);
        }

        public static void SafeInvoke<T>(this T isi, Action<T> call) where T : ISynchronizeInvoke
        {
            if (isi.InvokeRequired) isi.BeginInvoke(call, new object[] { isi });
            else
                call(isi);
        }
    }

    public static class AllExtensions
    {
        public static void SaveToFile<T>(this T isi, string Pfad)
        {
            Type t = isi.GetType();
            XmlSerializer xmls = new XmlSerializer(t);
            FileStream fs = new FileStream(Pfad, FileMode.Create);
            xmls.Serialize(fs, isi);
            fs.Close();
        }

        public static void LoadFromFile<T>(this T isi, string Pfad, ref T data)
        {
            XmlSerializer xmls = new XmlSerializer(isi.GetType());
            FileStream fs = new FileStream(Pfad, FileMode.Open);
            data = (T) xmls.Deserialize(fs);
            fs.Close();
        }

        public static byte[] ToByteArrayXML<T>(this T isi)
        {
            Type t = isi.GetType();
            XmlSerializer xmls = new XmlSerializer(t);
            MemoryStream st = new MemoryStream();
            xmls.Serialize(st, isi);
            return st.ToArray();
        }

        public static T FromByteArrayXML<T>(this byte[] arr)
        {
            XmlSerializer xmls = new XmlSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(arr);
            return (T)xmls.Deserialize(ms);
        }
        /*

        public static void LoadFromBinFile<T>(this T isi, string Pfad, ref T data)
        {
            SharpSerializer ss = new SharpSerializer(true);
            data = (T) ss.Deserialize(Pfad);
        }

        public static void LoadFromBinFile<T>(this T isi, string Pfad)
        {
            SharpSerializer ss = new SharpSerializer(true);
            T data = (T) ss.Deserialize(Pfad);
            PropertyInfo[] pin = data.GetType().GetProperties();
            foreach (PropertyInfo pi in isi.GetType().GetProperties())
            {
                pi.SetValue(isi, pin.First(d => d.Name == pi.Name).GetValue(data, null), null);
            }
            FieldInfo[] fin = data.GetType().GetFields();
            foreach (FieldInfo fi in isi.GetType().GetFields())
            {
                fi.SetValue(isi, fin.First(d => d.Name == fi.Name).GetValue(data));
            }
        }

        public static void SaveToBinFile<T>(this T isi, string Pfad)
        {
            SharpSerializer ss = new SharpSerializer(true);
            ss.Serialize(isi, Pfad);
        }*/

        public static byte[] ToByteArray<T>(this T isi)
        {
            if (isi == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            bf.Serialize(ms, isi);
            return ms.ToArray();
        }

        public static T FromByteArray<T>(this byte[] arr)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();
            memStream.Write(arr, 0, arr.Length);
            memStream.Seek(0, SeekOrigin.Begin);
            T obj = (T)binForm.Deserialize(memStream);
            return obj;
        }
    }
}
