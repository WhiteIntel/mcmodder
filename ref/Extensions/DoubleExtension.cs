﻿using System;

namespace Extensions
{
    public static class DoubleExtension
    {
        public static int ToInt32(this double d)
        {
            return Convert.ToInt32(d);
        }
        /// <summary>
        /// Konvertiert die Zahl in eine für Menschen leicht leserliche Zahl  1 KB, 1 MB, 1 GB
        /// </summary>
        /// <param name="zahl">Zahl</param>
        /// <returns>leserliche Zahl</returns>
        public static string Groesse(double zahl)
        {
            int i = 0;
            while (zahl > 1024)
            {
                if (zahl > 1024)
                {
                    zahl /= 1024.0;
                }
                i++;
            }
            string endung = "";
            if (i == 0)
                endung = "byte";
            else if (i == 1)
                endung = "KB";
            else if (i == 2)
                endung = "MB";
            else if (i == 3)
                endung = "GB";
            else if (i == 4)
                endung = "TB";
            try
            {
                return string.Format("{0:0.00} {1}", zahl, endung);
            }
            catch
            {
                return endung;
            }
        }
    }
}