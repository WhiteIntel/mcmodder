﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace Extensions
{
    public static class SocketExtensions
    {
        /// <summary>
        /// Trys to Receive String Encoding UTF-8 
        /// No Exception
        /// </summary>
        /// <param name="socket"></param>
        /// <returns></returns>
        public static string TryReceiveString(this Socket socket)
        {
            try
            {
                byte[] data = new byte[1024*1024];
                int len = socket.Receive(data);
                return Encoding.UTF8.GetString(data, 0, len);
            }
            catch
            {
                return "";
            }
        }
        /// <summary>
        /// Receive String Encoding UTF-8
        /// </summary>
        /// <param name="socket"></param>
        /// <returns></returns>
        public static string ReceiveString(this Socket socket)
        {
            byte[] data = new byte[1024*1024];
            int len = socket.Receive(data);
            return Encoding.UTF8.GetString(data, 0, len);
        }
        /// <summary>
        /// Receive String
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="encoding">Encoding</param>
        /// <returns></returns>
        public static string ReceiveString(this Socket socket, Encoding encoding)
        {
            byte[] data = new byte[4096];
            int len = socket.Receive(data);
            return encoding.GetString(data,0,len);
        }
        /// <summary>
        /// Send String
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="msg">String</param>
        public static void SendString(this Socket socket, string msg)
        {
            byte[] data = Encoding.UTF8.GetBytes(msg);
            socket.Send(data);
        }
        /// <summary>
        /// Send String
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="msg">String</param>
        /// <param name="encoding">Encoding</param>
        public static void SendString(this Socket socket, string msg, Encoding encoding)
        {
            byte[] data = encoding.GetBytes(msg);
            socket.Send(data);
        }
        /// <summary>
        /// Senden einens Objektes
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="socket"></param>
        /// <param name="obj">Objekt</param>
        public static void SendObject<T>(this Socket socket, T obj)
        {
            socket.Send(obj.ToByteArray());
        }
        /// <summary>
        /// Empfangen eines Objektes
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="socket"></param>
        /// <returns>Objekt</returns>
        public static T ReceiveObject<T>(this Socket socket)
        {
            List<byte> bytes = new List<byte>();
            byte[] data = new byte[4096];
            int len = 0;
            if((len = socket.Receive(data)) > 0)
            {
                bytes.AddRange(data.SubArray(0, len));
            }
            return bytes.ToArray().FromByteArray<T>();
        }

        public static void SendHtml(this Socket socket, string htm)
        {
            int len = Encoding.UTF8.GetByteCount(htm);
            string html = "HTTP/1.1 200 OK\r\n" +
                          "Server: CRauterich (Windows) C#4.5\r\n" +
                          "Content-Length: "+len+"\r\n" +
                          "Content-Language: de\r\n" +
                          "Connection: close\r\n" +
                          "Content-Type: text/html\r\n\r\n";
            html += htm;
            socket.SendString(html);

        }
    }
}
