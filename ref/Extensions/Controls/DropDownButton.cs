﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Extensions.Controls
{
    public partial class DropDownButton : UserControl
    {
        public DropDownButton()
        {
            InitializeComponent();
        }

        public new string Text 
        {
            get
            {
                return button.Text;
            }
            set
            {
                button.Text = value;
            } 
        }

        public new Color ForeColor
        {
            get
            {
                return button.ForeColor;
            }
            set
            {
                button.ForeColor = value;
            }
        }

        public new Font Font
        {
            get
            {
                return button.Font;
            }
            set { button.Font = value; }
        }

        public void Add(Control item)
        {
            panel.Controls.Add(item);
        }

        public void RemoveAt(int index)
        {
            panel.Controls.RemoveAt(index);
        }

        private void button_SizeChanged(object sender, EventArgs e)
        {
            this.Size = button.Size;
        }

        private void button_Click(object sender, EventArgs e)
        {
            panel.AutoSize = !panel.AutoSize;
        }
    }
}
