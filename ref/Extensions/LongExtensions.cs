﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Extensions
{
    public static class LongExtensions
    {
        /// <summary>
        /// Konvertiert die Zahl in eine für Menschen leicht leserliche Zahl  1 KB, 1 MB, 1 GB
        /// </summary>
        /// <param name="zahl">Zahl</param>
        /// <returns>leserliche Zahl</returns>
        public static string Groesse(this long zahl)
        {
            int i = 0;
            double wert = zahl;
            while (zahl > 1024)
            {
                if (zahl > 1024 * 1024)
                {
                    zahl /= 1024;
                }
                else
                {
                    wert = zahl / 1024.0;
                    zahl = 0;
                }
                i++;
            }
            string endung = "";
            if (i == 0)
                endung = "byte";
            else if (i == 1)
                endung = "KB";
            else if (i == 2)
                endung = "MB";
            else if (i == 3)
                endung = "GB";
            else if (i == 4)
                endung = "TB";
            try
            {
                return string.Format("{0:0.00} {1}", wert, endung);
            }
            catch
            {
                return endung;
            }
        }

        public static string SekudenZuZeitString(this long sekunden)
        {
            int min = Convert.ToInt32(sekunden / 60);
            int h = min / 60;
            if (h > 0)
            {
                min %= 60;
                sekunden %= 60;
                return string.Format("{0}h {1}m {2}s", h, min, sekunden);
            }
            else if (min > 0)
            {
                sekunden %= 60;
                return string.Format("{0}m {1}s", min, sekunden);
            }
            return string.Format("{0}s", sekunden);
        }

        public static int ToInt32(this long l)
        {
            return Convert.ToInt32(l);
        }
    }
}
