﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Extensions;

namespace EasyWeb
{
    public class EasyDownloader
    {
        public string Link { get; set; }
        public string Pfad { get; set; }
        public string Geschwindigkeit { get; set; }
        public string Heruntergeladen { get; set; }
        public string ZuLaden { get; set; }
        public string Dauer { get; set; }
        public int Prozent { get; set; }
        public bool Fertig { get; set; }
        public long zuladen { get; set; }
        public long heruntergeladen { get; set; }
        public long dauer;
        public long geschwindigkeit;
        private long curgeschw;
        private DateTime dt;
        private Task task;
        public CookieContainer Cookies { get; set; }
        public long Maxdlspeed { get; set; }
        private bool stop;

        public EasyDownloader(string link, string pfad)
        {
            Link = link;
            Pfad = pfad;
            Cookies = new CookieContainer();
        }

        public EasyDownloader()
        {
            
        }

        public void Start()
        {
            Fertig = false;
            task = Task.Factory.StartNew(DoDownload);
        }

        private void DoDownload()
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(Pfad));
                HttpWebRequest request = (HttpWebRequest) WebRequest.Create(Link);
                request.KeepAlive = true;
                request.Timeout = 5000;
                request.UserAgent = ".NET Framework/4.0";
                request.CookieContainer = Cookies;
                FileStream filestream = null;
                if (heruntergeladen > 0)
                {
                    filestream = new FileStream(Pfad, FileMode.Append);
                    request.AddRange(filestream.Length);
                    filestream.Seek(filestream.Length, SeekOrigin.Begin);
                    heruntergeladen = filestream.Length;
                }
                HttpWebResponse response = (HttpWebResponse) request.GetResponse();
                long contlen = response.ContentLength;
                if (heruntergeladen <= 0)
                {
                    zuladen = contlen;
                    ZuLaden = zuladen.Groesse();
                    filestream = new FileStream(Pfad, FileMode.Create);
                }

                Stream webstream = response.GetResponseStream();

                int len = 0;
                byte[] data = new byte[1024];
                DateTime time = DateTime.Now;
                while ((len = webstream.Read(data, 0, 1024)) > 0 && !stop)
                {
                    heruntergeladen += len;
                    curgeschw += len;
                    filestream.Write(data, 0, len);
                    if ((DateTime.Now - dt).TotalMilliseconds > 500)
                    {
                        curgeschw *= 2;
                        dt = DateTime.Now;
                        Heruntergeladen = heruntergeladen.Groesse();
                        Geschwindigkeit = curgeschw.Groesse() + "/s";
                        geschwindigkeit = curgeschw;
                        if (curgeschw > 0)
                        {
                            dauer = (zuladen - heruntergeladen)/curgeschw;
                            Dauer = dauer.SekudenZuZeitString();
                        }
                        if (heruntergeladen > 0 && zuladen > 0)
                        {
                            Prozent = (100*heruntergeladen/zuladen).ToInt32();
                            OnFortschritt(Prozent);
                        }
                        curgeschw = 0;
                    }
                    if (Maxdlspeed > 0)
                    {
                        int wait = (int) (1000/Maxdlspeed);
                        Thread.Sleep(wait);
                    }
                }
                Heruntergeladen = heruntergeladen.Groesse();
                filestream.Close();
                stop = false;
                geschwindigkeit = 0;
                dauer = 0;
                if (heruntergeladen == zuladen)
                {
                    Prozent = 100;
                    Geschwindigkeit = "Fertig";
                    Dauer = "Fertig";
                    Fertig = true;
                    OnFertig();
                }
                else
                {
                    Geschwindigkeit = geschwindigkeit.Groesse() + "/s";
                }
            }
            catch
            {
            }
        }

        public void Pause()
        {
            stop = true;
            task.Wait();
        }

        public void Stop()
        {
            stop = true;
            heruntergeladen = 0;
        }

        void OnFortschritt(int prozent)
        {
            FortschrittEventHandler myEvent = Fortschritt;
            if (myEvent != null)
            {
                myEvent(this, prozent);
            }
        }

        void OnFertig()
        {
            FertigEventHandler myEvent = IstFertig;
            if (myEvent != null)
            {
                myEvent(this);
            }
        }

        public event FortschrittEventHandler Fortschritt;
        public event FertigEventHandler IstFertig;
    }

    public delegate void FortschrittEventHandler(EasyDownloader sender, int prozent);
    public delegate void FertigEventHandler(EasyDownloader sender);
}