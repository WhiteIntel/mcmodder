﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EasyWeb
{
    public class EasyAPI
    {
        public static List<T> JSONObjectParse<T>(string url)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.KeepAlive = true;
            webRequest.Method = "GET";
            webRequest.ContentType = "text/html";
            webRequest.Accept = "text/html,application/xhtml+xml,application/xml,application/json";
            webRequest.KeepAlive = true;
            webRequest.Timeout = 5000;
            webRequest.UserAgent = ".NET Framework/4.0";
            webRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            webRequest.Headers.Add("Accept-Language: de-de");
            webRequest.Method = "GET";
            HttpWebResponse webResponse = null;
            webResponse = (HttpWebResponse)webRequest.GetResponse();
            Encoding e = Encoding.UTF8;
            try
            {
                e = Encoding.GetEncoding(webResponse.CharacterSet);
            }
            catch
            {
            }
            string json = new StreamReader(webResponse.GetResponseStream(), e).ReadToEnd();
            return null;
        }
    }
}
