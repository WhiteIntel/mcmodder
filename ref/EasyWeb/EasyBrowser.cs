﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.Security.Cryptography.X509Certificates;

namespace EasyWeb
{
    public class EasyBrowser
    {
        public string cookie = "";
        public string Url { get; set; }
        public string Quelltext { get; private set; }
        public HtmlDocument Dokument { get; set; }
        public List<string> BildLinks { get; set; }
        public List<string> Links { get; set; }
        public int Timeout { get; set; }
        public X509CertificateCollection Zertifikate { get; set; }
        public CookieContainer cookieContainer = new CookieContainer();
        public string Titel { get; set; }

        public EasyBrowser Clone()
        {
            return new EasyBrowser {Timeout = Timeout, Zertifikate = Zertifikate, cookieContainer = cookieContainer};
        }

        public EasyBrowser()
        {
            Zertifikate = new X509CertificateCollection();
            Timeout = -1;
            Links = new List<string>();
            BildLinks = new List<string>();
            Timeout = 5000;
        }

        public static bool CheckOnline(string url)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create(url);
                webRequest.UserAgent = ".NET Framework/4.0";
                webRequest.Headers.Add("Accept-Language: de-de");
                webRequest.Method = "GET";
                HttpWebResponse webResponse = (HttpWebResponse) webRequest.GetResponse();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<HtmlNode> GetNodes()
        {
            return GetNodes(Dokument.DocumentNode);
        }

        public void Navigate(string Url, bool unform)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create(Url);
                webRequest.ClientCertificates.AddRange(Zertifikate);
                webRequest.KeepAlive = true;
                webRequest.Timeout = Timeout;
                webRequest.UserAgent = ".NET Framework/4.0";
                //webRequest.UserAgent =
                //    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22";
                webRequest.Headers.Add("Accept-Language: de-de");
                webRequest.Method = "GET";
                webRequest.CookieContainer = cookieContainer;
                if (cookie != "")
                {
                    //webRequest.Headers.Add(HttpRequestHeader.Cookie, cookie);
                }
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                Encoding e = Encoding.GetEncoding(webResponse.CharacterSet);
                if (cookie == "")
                {
                    //cookie = webResponse.Headers["Set-Cookie"];
                }
                this.Quelltext = new StreamReader(webResponse.GetResponseStream(), e).ReadToEnd();
                this.Url = webResponse.ResponseUri.ToString();
                Dokument = new HtmlDocument();
                Dokument.LoadHtml(this.Quelltext);
                ContentParser();
                Titel = Quelltext.Remove(0, Quelltext.IndexOf("<title>") + 7);
                Titel = Titel.Remove(Titel.IndexOf("</title>"));
            }
            catch { }
        }

        public void Navigate(string Url)
        {
            try
            {
                this.Url = Url;
                if (!Url.Contains("://"))
                    Url = "http://" + Url;
                Uri uri = new Uri(Url);
                HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create(uri);
                webRequest.KeepAlive = true;
                webRequest.Method = "GET";
                webRequest.ContentType = "text/html";
                webRequest.Accept = "text/html,application/xhtml+xml,application/xml";
                //webRequest.Headers.Add("Accept-Encoding", "gzip, deflate");
                //webRequest.Headers.Add("Accept-Language", "en-US,en;q=0.5");
                webRequest.ClientCertificates.AddRange(Zertifikate);
                webRequest.KeepAlive = true;
                webRequest.Timeout = Timeout;
                webRequest.UserAgent = ".NET Framework/4.0";
                webRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                //webRequest.UserAgent =
                //    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22";
                webRequest.Headers.Add("Accept-Language: de-de");
                webRequest.Method = "GET";
                webRequest.CookieContainer = cookieContainer;
                HttpWebResponse webResponse = null;
                try
                {
                    webResponse = (HttpWebResponse) webRequest.GetResponse();
                    this.Status = webResponse.StatusCode;
                }
                catch
                {
                }
                Encoding e = Encoding.UTF8;
                try
                {
                    e = Encoding.GetEncoding(webResponse.CharacterSet);
                }
                catch
                {
                }
                this.Quelltext = new StreamReader(webResponse.GetResponseStream(), e).ReadToEnd();
                this.Url = webResponse.ResponseUri.ToString();
                Dokument = new HtmlDocument();
                Dokument.LoadHtml(this.Quelltext);
                ContentParser();
                Titel = Quelltext.Remove(0, Quelltext.IndexOf("<title>") + 7);
                Titel = Titel.Remove(Titel.IndexOf("</title>"));
            }
            catch(Exception e)
            {
                this.Fehler = e.ToString();
            }
        }

        public HttpStatusCode Status { get; set; }

        public string Fehler { get; set; }

        public string GetContentType(string Url)
        {
            try
            {
                this.Url = Url;
                if (!Url.StartsWith("http"))
                    Url = "http://" + Url;
                Uri uri = new Uri(Url);
                HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create(uri);
                webRequest.ClientCertificates.AddRange(Zertifikate);
                webRequest.KeepAlive = true;
                webRequest.Timeout = -1;
                webRequest.UserAgent = ".NET Framework/4.0";
                //webRequest.UserAgent =
                //    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22";
                webRequest.Headers.Add("Accept-Language: de-de");
                webRequest.Method = "GET";
                webRequest.CookieContainer = cookieContainer;
                if (cookie != "")
                {
                    //webRequest.Headers.Add(HttpRequestHeader.Cookie, cookie);
                }
                HttpWebResponse webResponse = (HttpWebResponse) webRequest.GetResponse();
                return webResponse.ContentType;
            }
            catch
            {
            }
            return null;
        }

        public string GetFileName(string url)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.Timeout = Timeout;
            req.CookieContainer = cookieContainer;
            req.UserAgent = ".NET Framework/4.0";
            WebResponse response = req.GetResponse();
            string originalFileName = response.Headers["Content-Disposition"];
            return originalFileName.Split('\"')[1];
        }

        public string GetContentHeader(string Url)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);
            req.Method = "HEAD";
            req.Timeout = Timeout;
            req.ReadWriteTimeout = Timeout;
            req.CookieContainer = cookieContainer;
            req.UserAgent = ".NET Framework/4.0";
            HttpWebResponse resp = (HttpWebResponse)(req.GetResponse());
            return new StreamReader(resp.GetResponseStream()).ReadToEnd();
        }

        public long GetContentLength(string Url)
        {
            try
            {
                if (!Url.StartsWith("http"))
                    Url = "http://" + Url;
                Uri uri = new Uri(Url);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
                webRequest.Timeout = Timeout;
                webRequest.UserAgent = ".NET Framework/4.0";
                webRequest.KeepAlive = true;
                webRequest.CookieContainer = cookieContainer;
                //webRequest.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)";
                //webRequest.Headers.Add("Accept-Language: de-de");
                WebResponse webResponse = webRequest.GetResponse();
                return webResponse.ContentLength;
            }
            catch { }
            return -1;
        }

        void ContentParser()
        {
            BildLinks.Clear();
            Links.Clear();
            NodeParse(this.Dokument.DocumentNode);
        }

        public List<HtmlNode> GetNodes(HtmlNode hn)
        {
            List<HtmlNode> nodes = new List<HtmlNode>();
            nodes.Add(hn);
            foreach (HtmlNode childNode in hn.ChildNodes)
            {
                nodes.AddRange(GetNodes(childNode));
            }
            return nodes;
        } 

        public List<HtmlNode> GetFormNodes()
        {
            List<HtmlNode> nodes = new List<HtmlNode>();
            foreach (HtmlNode childNode in Dokument.DocumentNode.ChildNodes)
            {
                nodes.Add(childNode);
                nodes.AddRange(GetNodes(childNode));
            }
            return nodes.Where(d=>d.Name == "form").ToList();
        }

        void NodeParse(HtmlNode node)
        {
            if (node.Name.ToLower() == "img")
            {
                if (node.Attributes.Contains("src"))
                {
                    string link = node.Attributes["src"].Value;
                    if (link.StartsWith("//"))
                    {
                        link = "http:" + link;
                    }
                    if (!link.StartsWith("http"))
                    {
                        Uri ur = new Uri(this.Url);
                        link = ur.Host + link;
                    }
                    BildLinks.Add(link);
                }
            }
            if (node.Name.ToLower() == "a")
            {
                if (node.Attributes.Contains("href"))
                {
                    string link = node.Attributes["href"].Value;
                    if (!link.StartsWith("http"))
                    {
                        if (link.StartsWith("/"))
                        {
                            Uri ur = new Uri(this.Url);
                            link = ur.Host + link;
                        }
                        else
                        {
                            Uri ur = new Uri(this.Url);
                            string temp = ur.Host;
                            for (int i = 0; i < ur.Segments.Length - 1; i++)
                            {
                                temp += ur.Segments[i];
                            }
                            if (ur.Segments.Last().EndsWith("/"))
                            {
                                temp += ur.Segments.Last();
                            }
                            link = temp + link;
                        }
                    }
                    if (link.StartsWith("//"))
                    {
                        link = "http:" + link;
                    }
                    Links.Add(link);
                }
            }
            foreach (HtmlNode n in node.ChildNodes)
            {
                if (n != null)
                    NodeParse(n);
            }
        }

        public Image DownloadImg(string Url)
        {
            try
            {
                Uri uri = new Uri(Url);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
                webRequest.CookieContainer = cookieContainer;
                webRequest.UserAgent = ".NET Framework/4.0";
                webRequest.Headers.Add("Accept-Language: de-de");
                webRequest.Method = "GET";
                webRequest.Timeout = Timeout;
                Image bild = Image.FromStream(webRequest.GetResponse().GetResponseStream());
                return bild;
            }
            catch { }
            return null;
        }

        public static Stream DownloadStream(string Url)
        {
            try
            {
                Uri uri = new Uri(Url);
                HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create(uri);
                webRequest.UserAgent = ".NET Framework/4.0";
                webRequest.Headers.Add("Accept-Language: de-de");
                webRequest.Method = "GET";
                webRequest.Timeout = 5000;
                return webRequest.GetResponse().GetResponseStream();
            }
            catch
            {
            }
            return null;
        }

        public static Image DownloadBild(string Url, int Timeout)
        {
            try
            {
                Uri uri = new Uri(Url);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
                webRequest.UserAgent = ".NET Framework/4.0";
                webRequest.Headers.Add("Accept-Language: de-de");
                webRequest.Method = "GET";
                webRequest.Timeout = Timeout;
                Image bild = Image.FromStream(webRequest.GetResponse().GetResponseStream());
                return bild;
            }
            catch { }
            return null;
        }

        public static Image DownloadBild(string Url)
        {
            return DownloadBild(Url, 1000);
        }

        public static void StaticDownloadToFile(string Url, string Pfad, int Timeout)
        {
            try
            {
                Uri uri = new Uri(Url);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
                //webRequest.Headers.Add(HttpRequestHeader.Cookie, cookie);
                webRequest.UserAgent = ".NET Framework/4.0";
                webRequest.Headers.Add("Accept-Language: de-de");
                webRequest.Method = "GET";
                webRequest.Timeout = Timeout;
                FileStream f = new FileStream(Pfad, FileMode.Create);
                webRequest.GetResponse().GetResponseStream().CopyTo(f);
                f.Close();
            }
            catch { }
        }

        public void DownloadToFile(string Url, string Pfad)
        {
            try
            {
                Uri uri = new Uri(Url);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
                webRequest.CookieContainer = cookieContainer;
                webRequest.Headers.Add(HttpRequestHeader.Cookie, cookie);
                webRequest.UserAgent = ".NET Framework/4.0";
                webRequest.Headers.Add("Accept-Language: de-de");
                webRequest.Method = "GET";
                FileStream f = new FileStream(Pfad, FileMode.Create);
                webRequest.GetResponse().GetResponseStream().CopyTo(f);
                f.Close();
            }
            catch { }
        }
        /// <summary>
        /// Soap+XML
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="Data"></param>
        public void Post(string Url, string Data)
        {
            try
            {
                this.Url = Url;
                Uri uri = new Uri(this.Url);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
                if (cookie != "")
                    //webRequest.Headers.Add("Cookie", cookie);
                    webRequest.Method = "POST";
                webRequest.ContentType = "application/soap+xml; charset=utf-8";
                webRequest.CookieContainer = cookieContainer;

                byte[] byteArray = Encoding.UTF8.GetBytes(Data);
                webRequest.ContentLength = byteArray.Length;

                Stream stream = webRequest.GetRequestStream();
                stream.Write(byteArray, 0, byteArray.Length);
                stream.Close();

                WebResponse webResponse = webRequest.GetResponse();
                cookie = webResponse.Headers["Set-cookie"];
                this.Quelltext = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
                Dokument = new HtmlDocument();
                Dokument.LoadHtml(this.Quelltext);
                ContentParser();
            }
            catch { }
        }

        public void PostString(string Url, string Data)
        {
            try
            {
                this.Url = Url;
                Uri uri = new Uri(this.Url);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
                //if (cookie != "")
                    //webRequest.Headers.Add("Cookie", cookie);
                webRequest.Method = "POST";
                webRequest.ContentType = "text/html; charset=utf-8";
                webRequest.CookieContainer = cookieContainer;

                byte[] byteArray = Encoding.UTF8.GetBytes(Data);
                webRequest.ContentLength = byteArray.Length;

                Stream stream = webRequest.GetRequestStream();
                stream.Write(byteArray, 0, byteArray.Length);
                stream.Close();

                WebResponse webResponse = webRequest.GetResponse();
                this.Quelltext = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
                Dokument = new HtmlDocument();
                Dokument.LoadHtml(this.Quelltext);
                ContentParser();
            }
            catch { }
        }

        public void Post(string Url, List<PostParameter> postParameterCol, Encoding encoding)
        {
            try
            {
                this.Url = Url;
                string post = "";
                foreach (PostParameter pp in postParameterCol)
                {
                    post += pp.Name + "=" + pp.Text + "&";
                }
                post = post.TrimEnd('&');
                Uri uri = new Uri(this.Url);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
                webRequest.UserAgent = ".NET Framework/4.0";
                webRequest.CookieContainer = cookieContainer;
                webRequest.KeepAlive = true;
                //if (cookie != "")
                //webRequest.Headers.Add("Cookie", cookie);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/x-www-form-urlencoded";

                byte[] byteArray = encoding.GetBytes(post);
                webRequest.ContentLength = byteArray.Length;

                Stream stream = webRequest.GetRequestStream();
                stream.Write(byteArray, 0, byteArray.Length);
                stream.Close();

                WebResponse webResponse = webRequest.GetResponse();
                //cookie = webResponse.Headers["Set-cookie"];
                this.Quelltext = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
                Dokument = new HtmlDocument();
                Dokument.LoadHtml(this.Quelltext);
                ContentParser();
            }
            catch
            {
            }
        }

        public void Post(string Url, List<PostParameter> postParameterCol)
        {
            try
            {
                this.Url = Url;
                string post = "";
                foreach (PostParameter pp in postParameterCol)
                {
                    post += pp.Name + "=" + pp.Text + "&";
                }
                post = post.TrimEnd('&');
                Uri uri = new Uri(this.Url);
                HttpWebRequest webRequest = (HttpWebRequest) WebRequest.Create(uri);
                webRequest.UserAgent = ".NET Framework/4.0";
                webRequest.CookieContainer = cookieContainer;
                if (cookie != "")
                    webRequest.Headers.Add("Cookie", cookie);
                webRequest.Method = "POST";
                webRequest.Timeout = Timeout;
                webRequest.ContentType = "application/x-www-form-urlencoded";

                byte[] byteArray = Encoding.UTF8.GetBytes(post);
                webRequest.ContentLength = byteArray.Length;

                Stream stream = webRequest.GetRequestStream();
                stream.Write(byteArray, 0, byteArray.Length);
                stream.Close();

                WebResponse webResponse = webRequest.GetResponse();
                cookie = webResponse.Headers["Set-cookie"];
                this.Quelltext = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
                Dokument = new HtmlDocument();
                Dokument.LoadHtml(this.Quelltext);
                ContentParser();
            }
            catch
            {
            }
        }

        public string GetResponseUrl(string Url)
        {
            Uri uri = new Uri(Url);
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(uri);
            webRequest.CookieContainer = cookieContainer;
            webRequest.UserAgent = ".NET Framework/4.0";
            webRequest.Headers.Add("Accept-Language: de-de");
            webRequest.Method = "GET";
            webRequest.Timeout = Timeout;
            WebResponse wr = webRequest.GetResponse();
            Url = wr.ResponseUri.ToString();
            return Url;
        }

        public HtmlNode GetFirstForm()
        {
            if (this.Dokument != null)
                return GFF(this.Dokument.DocumentNode);
            return null;
        }

        HtmlNode GFF(HtmlNode dn)
        {
            foreach (HtmlNode h in dn.ChildNodes)
            {
                if (h.Name.ToLower() == "form")
                {
                    return h;
                }
                HtmlNode n = GFF(h);
                if (n != null)
                    return n;
            }
            return null;
        }

        public HtmlNode GetNodeByClass(string NodeTyp, string Classname)
        {
            if (this.Dokument != null)
                return GNBC(this.Dokument.DocumentNode, NodeTyp.ToLower(), Classname);
            return null;
        }

        HtmlNode GNBC(HtmlNode dn, string NT, string CN)
        {
            foreach (HtmlNode h in dn.ChildNodes)
            {
                if (h.Name.ToLower() == NT && h.Attributes["class"] != null && h.Attributes["class"].Value == CN)
                {
                    return h;
                }
                HtmlNode n = GNBC(h, NT, CN);
                if (n != null)
                    return n;
            }
            return null;
        }
    }

    public class PostParameter
    {
        public string Name { get; set; }
        public string Text { get; set; }

        public PostParameter(string name, string text)
        {
            Name = name;
            Text = text;
        }

        public PostParameter() { }
    }
}
