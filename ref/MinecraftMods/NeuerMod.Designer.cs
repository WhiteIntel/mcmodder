﻿namespace MinecraftMods
{
    partial class NeuerMod
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.suchBox = new System.Windows.Forms.TextBox();
            this.suchenButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ergebnisBox = new System.Windows.Forms.ComboBox();
            this.okButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lokalVersion = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.speichern = new System.Windows.Forms.Button();
            this.abbrechen = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.modFile = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.modName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dateiButton = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.onlineVersionen = new System.Windows.Forms.ListBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name:";
            // 
            // suchBox
            // 
            this.suchBox.Location = new System.Drawing.Point(80, 14);
            this.suchBox.Name = "suchBox";
            this.suchBox.Size = new System.Drawing.Size(240, 20);
            this.suchBox.TabIndex = 1;
            // 
            // suchenButton
            // 
            this.suchenButton.Location = new System.Drawing.Point(326, 12);
            this.suchenButton.Name = "suchenButton";
            this.suchenButton.Size = new System.Drawing.Size(75, 23);
            this.suchenButton.TabIndex = 2;
            this.suchenButton.Text = "Suchen";
            this.suchenButton.UseVisualStyleBackColor = true;
            this.suchenButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ergebnisBox);
            this.groupBox1.Controls.Add(this.okButton);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.suchenButton);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.suchBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(413, 71);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Suche";
            // 
            // ergebnisBox
            // 
            this.ergebnisBox.FormattingEnabled = true;
            this.ergebnisBox.Location = new System.Drawing.Point(80, 39);
            this.ergebnisBox.Name = "ergebnisBox";
            this.ergebnisBox.Size = new System.Drawing.Size(240, 21);
            this.ergebnisBox.TabIndex = 6;
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(326, 37);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 5;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ergebnisse:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.onlineVersionen);
            this.groupBox2.Controls.Add(this.dateiButton);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.lokalVersion);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.modFile);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.modName);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(413, 183);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mod Eintrag";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Lokal:";
            // 
            // lokalVersion
            // 
            this.lokalVersion.BackColor = System.Drawing.SystemColors.Control;
            this.lokalVersion.Location = new System.Drawing.Point(80, 71);
            this.lokalVersion.Name = "lokalVersion";
            this.lokalVersion.ReadOnly = true;
            this.lokalVersion.Size = new System.Drawing.Size(70, 20);
            this.lokalVersion.TabIndex = 8;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.speichern);
            this.groupBox3.Controls.Add(this.abbrechen);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(3, 120);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(407, 60);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            // 
            // speichern
            // 
            this.speichern.Dock = System.Windows.Forms.DockStyle.Right;
            this.speichern.Location = new System.Drawing.Point(254, 16);
            this.speichern.Name = "speichern";
            this.speichern.Size = new System.Drawing.Size(75, 41);
            this.speichern.TabIndex = 1;
            this.speichern.Text = "Speichern";
            this.speichern.UseVisualStyleBackColor = true;
            this.speichern.Click += new System.EventHandler(this.speichern_Click);
            // 
            // abbrechen
            // 
            this.abbrechen.Dock = System.Windows.Forms.DockStyle.Right;
            this.abbrechen.Location = new System.Drawing.Point(329, 16);
            this.abbrechen.Name = "abbrechen";
            this.abbrechen.Size = new System.Drawing.Size(75, 41);
            this.abbrechen.TabIndex = 0;
            this.abbrechen.Text = "Abbrechen";
            this.abbrechen.UseVisualStyleBackColor = true;
            this.abbrechen.Click += new System.EventHandler(this.abbrechen_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Datei:";
            // 
            // modFile
            // 
            this.modFile.BackColor = System.Drawing.SystemColors.Control;
            this.modFile.Location = new System.Drawing.Point(80, 45);
            this.modFile.Name = "modFile";
            this.modFile.ReadOnly = true;
            this.modFile.Size = new System.Drawing.Size(240, 20);
            this.modFile.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Name:";
            // 
            // modName
            // 
            this.modName.Location = new System.Drawing.Point(80, 19);
            this.modName.Name = "modName";
            this.modName.ReadOnly = true;
            this.modName.Size = new System.Drawing.Size(240, 20);
            this.modName.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(182, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Online:";
            // 
            // dateiButton
            // 
            this.dateiButton.Location = new System.Drawing.Point(326, 43);
            this.dateiButton.Name = "dateiButton";
            this.dateiButton.Size = new System.Drawing.Size(75, 23);
            this.dateiButton.TabIndex = 11;
            this.dateiButton.Text = "Datei ...";
            this.dateiButton.UseVisualStyleBackColor = true;
            this.dateiButton.Click += new System.EventHandler(this.dateiButton_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Jar|*.jar|Zip|*.zip";
            // 
            // onlineVersionen
            // 
            this.onlineVersionen.FormattingEnabled = true;
            this.onlineVersionen.Location = new System.Drawing.Point(228, 71);
            this.onlineVersionen.Name = "onlineVersionen";
            this.onlineVersionen.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.onlineVersionen.Size = new System.Drawing.Size(173, 43);
            this.onlineVersionen.TabIndex = 12;
            // 
            // NeuerMod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 254);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "NeuerMod";
            this.Text = "Neuer Mod";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox suchBox;
        private System.Windows.Forms.Button suchenButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox modName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox modFile;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button abbrechen;
        private System.Windows.Forms.Button speichern;
        private System.Windows.Forms.ComboBox ergebnisBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox lokalVersion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button dateiButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ListBox onlineVersionen;
    }
}