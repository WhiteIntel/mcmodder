﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using EasyWeb;
using Extensions;
using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MinecraftMods
{
    public static class UpdaterDB
    {
        public static List<Mod> Mods { get; set; }

        public static void Save()
        {
            Mods.SaveToFile("mods_update.xml");
        }

        public static void Load()
        {
            Mods = new List<Mod>();
            if (File.Exists("mods_update.xml"))
            {
                List<Mod> mods = new List<Mod>();
                mods.LoadFromFile("mods_update.xml", ref mods);
                Mods = mods;
            }
        }
    }

    public static class LokalDB
    {
        public static List<Mod> Mods { get; set; }

        public static void LoadMods()
        {
            Mods = new List<Mod>();
            string mcpath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                ".minecraft");
            List<string> files = Directory.GetFiles(Path.Combine(mcpath, "mods"), "*.jar").ToList();
            files.AddRange(Directory.GetFiles(Path.Combine(mcpath, "mods"), "*.zip"));
            foreach (string file in files)
            {
                ZipFile zf = new ZipFile(file);
                ZipEntry entry = zf.GetEntry("mcmod.info");
                if (entry != null)
                {
                    Stream stream = zf.GetInputStream(entry);
                    string s = new StreamReader(stream).ReadToEnd();
                    try
                    {
                        Mod m = new Mod();
                        m.Name = Regex.Match(s, "\\\"modid\\\"\\s*:\\s*\\\".+\\\"").Value.Split('\"')[3];
                        m.Version = Regex.Match(s, "\\\"version\\\"\\s*:\\s*\\\".+\\\"").Value.Split('\"')[3];
                        m.Datei = file;
                        //m.Name = s.Substring("\"modid\": \"", "\",");
                        //m.Version = s.Substring("\"version\": \"", "\",");
                        if(Mods.FirstOrDefault(d=>d.Name == m.Name)==null && m.Name != null)
                            Mods.Add(m);
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show("Fehler beim lesen:\r\n" + s);
                    }
                }
            }
        }

        public static void Save()
        {
            Mods.SaveToFile("mods.xml");
        }

        public static void Load()
        {
            if (File.Exists("mods.xml"))
            {
                List<Mod> mods = new List<Mod>();
                mods.LoadFromFile("mods.xml", ref mods);
                Mods = mods;
            }
        }
    }

    public static class OnlineDB
    {
        public static List<Mod> Mods { get; set; }

        public static void Load()
        {
            Mods = new List<Mod>();
            EasyBrowser eb = new EasyBrowser();
            eb.Navigate("http://bot.notenoughmods.com/?json");
            List<string> versions = new List<string>();
            string[] vs = eb.Quelltext.Split('\"');
            for (int i = 1; i < vs.Length; i += 2)
            {
                versions.Add(vs[i]);
            }
            Parallel.ForEach(versions, new ParallelOptions {MaxDegreeOfParallelism = 10}, version =>
            {
                EasyBrowser br = new EasyBrowser();
                br.Navigate("http://bot.notenoughmods.com/" + version + ".json");
                JContainer cont = (JContainer) JsonConvert.DeserializeObject(br.Quelltext);
                foreach (JToken mod in cont)
                {
                    List<JProperty> liste = mod.Cast<JProperty>().ToList();
                    Mod om = new Mod();
                    om.Name = (string) liste.First(d => d.Name == "name").Value;
                    om.Version = (string) liste.First(d => d.Name == "version").Value;
                    om.Url = (string) liste.First(d => d.Name == "longurl").Value;
                    om.MCVersion = version;
                    Mods.Add(om);
                }
            });
        }
    }

    public class Mod
    {
        public string Version { get; set; }
        public string MCVersion { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Datei { get; set; }
    }
}
