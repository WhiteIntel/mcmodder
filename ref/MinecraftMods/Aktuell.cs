﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MinecraftMods
{
    public partial class Aktuell : Form
    {
        public Aktuell()
        {
            InitializeComponent();
            LokalDB.LoadMods();
            LokalDB.Save();
            foreach (Mod mod in LokalDB.Mods)
            {
                dataGridView1.Rows.Add(mod.Name, mod.Version);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //try
            //{
                new NeuerMod((string) dataGridView1.SelectedRows[0].Cells[0].Value).ShowDialog();
            //}
            //catch
            //{
            //}
        }
    }
}
