﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using EasyWeb;
using ICSharpCode.SharpZipLib.Zip;

namespace MinecraftMods
{
    public partial class NeuerMod : Form
    {
        public NeuerMod()
        {
            InitializeComponent();
        }

        public NeuerMod(string mod)
        {
            InitializeComponent();
            suchBox.Text = mod;
            Suchen();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Suchen();
        }

        void Suchen()
        {
            string[] array =
                OnlineDB.Mods.Where(d => d.Name != null && d.Name.ToLower().Contains(suchBox.Text.ToLower()))
                    .Select(d => d.Name).ToArray();
            array = array.GroupBy(d => d).Select(d=>d.First()).ToArray();
            ergebnisBox.Items.Clear();
            ergebnisBox.Items.AddRange(array);
        }

        private void dateiButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                ZipFile zf = new ZipFile(openFileDialog1.FileName);
                ZipEntry entry = zf.GetEntry("mcmod.info");
                if (entry != null)
                {
                    Stream stream = zf.GetInputStream(entry);
                    string s = new StreamReader(stream).ReadToEnd();
                    try
                    {
                        lokalVersion.Text = Regex.Match(s, "\\\"version\\\"\\s*:\\s*\\\".+\\\"").Value.Split('\"')[3];
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show("Fehler beim lesen:\r\n" + s);
                    }
                }
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            List<Mod> array =
                OnlineDB.Mods.Where(d => d.Name != null && d.Name == ergebnisBox.SelectedItem.ToString()).ToList();
            modName.Text = ergebnisBox.SelectedItem.ToString();
            onlineVersionen.Items.Clear();
            onlineVersionen.Items.AddRange(array.Select(d => d.Version + "(" + d.MCVersion + ")").ToArray());

        }

        private void speichern_Click(object sender, EventArgs e)
        {
            Mod m = new Mod();
            m.Name = modName.Text;
            m.Datei = modFile.Text;
            m.Version = lokalVersion.Text;
            if (UpdaterDB.Mods.FirstOrDefault(d => d.Name == m.Name) == null)
                UpdaterDB.Mods.Add(m);
            UpdaterDB.Save();
            this.Close();
        }

        private void abbrechen_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
