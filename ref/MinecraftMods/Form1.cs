﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EasyWeb;
using Extensions;

namespace MinecraftMods
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Task.Factory.StartNew(() =>
            {
                EasyBrowser eb = new EasyBrowser();
                eb.Navigate("http://bot.notenoughmods.com/?json");
                List<string> versions = new List<string>();
                string[] vs = eb.Quelltext.Split('\"');
                for (int i = 1; i < vs.Length; i += 2)
                {
                    versions.Add(vs[i]);
                }
                versionBox.SafeInvoke(d => d.Items.AddRange(versions.ToArray()));
                UpdaterDB.Load();
                LokalDB.Load();
                OnlineDB.Load();
                foreach (Mod mod in UpdaterDB.Mods)
                {
                    modView.SafeInvoke(d=>d.Rows.Add(mod.Name, mod.Datei, mod.Version));
                }
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new NeuerMod().ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new Aktuell().ShowDialog();
        }
    }
}
