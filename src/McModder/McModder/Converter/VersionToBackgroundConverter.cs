﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using CommonExtensions.Enums;
using McModder.Interface;
using CommonExtensions;

namespace McModder
{
    internal class VersionToBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is IMinecraftMod)
            {
                var mod = value as IMinecraftMod;
                SoftwareVersionLevel level = mod.LocalVersion.CompareSoftwareVersions(mod.Version, new []{"Not available"});

                if (level == SoftwareVersionLevel.Equal)
                    return Brushes.LightGreen;
                else if (level == SoftwareVersionLevel.Higher)
                    return Brushes.Yellow;
                else if (level == SoftwareVersionLevel.Lower)
                    return Brushes.LightGreen;
                else if (level == SoftwareVersionLevel.Unknown)
                    return Brushes.Coral;
                //quantity should not be below 0
            }
            //value is not an integer. Do not throw an exception
            // in the converter, but return something that is obviously wrong
            return Brushes.Coral;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }  
}
