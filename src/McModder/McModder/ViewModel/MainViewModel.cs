﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using McModder.Interface;
using McModder.Service;
using Microsoft.Practices.Prism.Commands;
using Microsoft.WindowsAPICodePack.Dialogs;
using MS.WindowsAPICodePack.Internal;

namespace McModder.ViewModel
{
    internal class MainViewModel : BaseViewModel, IAsyncViewModel<EventArgs>
    {
        #region Field

        private IModService _service;
        private string _mcPath;
        private ObservableCollection<IMinecraftVersion> _mcVersions;
        private IMinecraftVersion _selectedVersion;
        private ObservableCollection<IMinecraftMod> _mcMods;
        private static object _lockObject = new object();
        #endregion
        public MainViewModel()
        {
            McPath = Utils.TryGetDefaultMcPath();
            _service = new ModService(McPath);
            BindingOperations.EnableCollectionSynchronization(McMods, _lockObject);
        }
        #region Properties
        public ObservableCollection<IMinecraftMod> McMods
        {
            get
            {
                if(_mcMods == null)
                    _mcMods = new ObservableCollection<IMinecraftMod>();
                return _mcMods;
            }
            set
            {
                if (value != null && value != _mcMods)
                {
                    _mcMods = value;
                    OnPropertyChanged("McMods");
                }
            }

        }
        public IMinecraftVersion SelectedVersion
        {
            get 
            {
                if (_selectedVersion == null)
                    _selectedVersion = McVersions.LastOrDefault();
                return _selectedVersion;
            }

            set
            {
                if (value != null && value != _selectedVersion)
                {
                    this._selectedVersion = value;
                    GetMods();
                    OnThisPropertyChanged();
                }
            }
        }
        public ObservableCollection<IMinecraftVersion> McVersions
        {
            get
            {
                if(_mcVersions == null)
                    _mcVersions = new ObservableCollection<IMinecraftVersion>();
                return _mcVersions;
            }
            set
            {
                if (value != null && value != _mcVersions)
                {
                    _mcVersions = value;
                    OnPropertyChanged("McVersions");
                }
            }
        }

        public string McPath
        {
            get
            {
                if (_mcPath == null)
                    _mcPath = "";
                return _mcPath;
            }

            set
            {
                if (value != null && value != _mcPath)
                {
                    _mcPath = value;
                    OnPropertyChanged("McPath");
                    _service = new ModService(value);
                    GetMods();
                }
            }
        }

        #endregion

        #region Commands
        public ICommand BrowseMcPathCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    var dlg = new CommonOpenFileDialog();
                    dlg.Title = "Select Mod folder";
                    dlg.IsFolderPicker = true;
                    dlg.InitialDirectory = _mcPath;

                    dlg.AddToMostRecentlyUsedList = false;
                    dlg.AllowNonFileSystemItems = false;
                    dlg.DefaultDirectory = _mcPath;
                    dlg.EnsureFileExists = true;
                    dlg.EnsurePathExists = true;
                    dlg.EnsureReadOnly = false;
                    dlg.EnsureValidNames = true;
                    dlg.Multiselect = false;
                    dlg.ShowPlacesList = true;

                    if (dlg.ShowDialog() == CommonFileDialogResult.Ok)
                    {
                        var folder = dlg.FileName;

                        McPath = folder;
                    }
                });
            }

        }

        public ICommand CheckUpdates
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    //TODO: Implement file dialog
                });
            }

        }

        #endregion

        private void GetMods()
        {
            if (_service != null && _selectedVersion != null)
            {
                Task.Run(() =>
                {
                    McMods =
                        new ObservableCollection<IMinecraftMod>(_service.GetMergedListAsync(_selectedVersion).Result);
                });
            }
        }

        public async Task InitializeAsync(EventArgs argument)
        {
            McVersions = new ObservableCollection<IMinecraftVersion>(_service.GetAvailableMinecraftVersionsAsync().Result);
            SelectedVersion = McVersions.LastOrDefault();
        }
    }
}
