﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using McModder.Interface;
using McModder.ViewModel;

namespace McModder.Extensions
{
    internal static class FrameworkExtensions
    {
        internal static void BindViewModel<T, E>(this FrameworkElement element) where T : BaseViewModel
        {
            if (element.DataContext == null)
            {
                var viewModel = Activator.CreateInstance<T>();
                IAsyncViewModel<E> asyncViewModel = viewModel as IAsyncViewModel<E>;
                if (asyncViewModel != null)
                {
                    element.Loaded += (s, e) =>
                    {
                        element.DataContext = viewModel;
                        var model = element.DataContext as IAsyncViewModel<E>;

                        if (model != null)
                            model.InitializeAsync(default(E));
                    };
                }
            }
        }
    }
}
