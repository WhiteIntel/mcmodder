﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CommonExtensions;
using McModder.Extensions;
using McModder.ViewModel;

namespace McModder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainViewModel _dataContext;
        public MainWindow()
        {
            InitializeComponent();
            this.BindViewModel<MainViewModel, EventArgs>();
        }

        private void HyperlinkClickHandler(object sender, RoutedEventArgs e)
        {
            e.OpenDefaultBrowser();
        }
    }
}
