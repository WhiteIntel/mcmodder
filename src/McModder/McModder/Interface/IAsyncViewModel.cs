﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McModder.Interface
{
    internal interface IAsyncViewModel<T>
    {
        Task InitializeAsync(T argument);
    }
}
