﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace McModder.Core
{
    public sealed class WebGrabber
    {
        public string GetJsonFromUrl(string url)
        {
            if(String.IsNullOrWhiteSpace(url))
                throw new ArgumentNullException("url");

            string result = "";
            using (WebClient client = new WebClient())
            {
                client.Proxy = null;
                //TODO: Implement Exception handling for bad url
                result = client.DownloadString(url);
            }
            return result;
        }

        public async Task<string> GetJsonFromUrlAsync(string url)
        {
            if (String.IsNullOrWhiteSpace(url))
                throw new ArgumentNullException("url");

            HttpClient client = new HttpClient();
            var response = client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode) {
                var getResponsestring = response.Content.ReadAsStringAsync().Result;
                return getResponsestring;
            }
            
            return "";
        }
    }
}
