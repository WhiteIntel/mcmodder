﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McModder.Core
{
    public sealed class LocalGrabber
    {
        private readonly string _modPath;
        public LocalGrabber(string modPath)
        {
            if(String.IsNullOrWhiteSpace(modPath))
                throw new ArgumentNullException("modPath");
            if(!Directory.Exists(modPath))
                throw new DirectoryNotFoundException(String.Format("The directory '{0}' does not exist!", modPath));
            _modPath = modPath;
        }

        /// <summary>
        /// Get all mods in the given directory. Default the file extension '*.jar' and '*.zip' are used.
        /// </summary>
        /// <returns></returns>
        public IList<string> GrabLocalMods()
        {
            return GrabLocalMods(new[] {"*.jar", "*.zip"});
        }

        /// <summary>
        /// Get all mods in the given directory and the given file extensions.
        /// </summary>
        /// <returns></returns>
        public IList<string> GrabLocalMods(string[] validFileExtensions)
        {
            List<string> mods = new List<string>();

            foreach (var validFileExtension in validFileExtensions)
            {
                mods.AddRange(Directory.GetFiles(_modPath, validFileExtension));
            }

            return mods;
        }
    }
}
