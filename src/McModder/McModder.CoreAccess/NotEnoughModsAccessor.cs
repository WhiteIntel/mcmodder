﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using McModder.CommonModels;
using McModder.Core;
using McModder.Interface;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace McModder.CoreAccess
{
    public class NotEnoughModsAccessor : IOnlineAccessor
    {
        private const string _baseUrl = "http://bot.notenoughmods.com";
        private readonly string _mcVersionUri = String.Format("{0}/{1}", _baseUrl, "?json");
        private WebGrabber _grabber;

        public NotEnoughModsAccessor()
        {
            _grabber = new WebGrabber();
        }
        public IList<IMinecraftVersion> GetMinecraftVersions()
        {
            List<IMinecraftVersion> result = new List<IMinecraftVersion>();
            string versions = _grabber.GetJsonFromUrl(_mcVersionUri);
            JContainer cont = (JContainer) JsonConvert.DeserializeObject(versions);

            foreach (var version in cont)
            {
                if (!String.IsNullOrWhiteSpace(version.Value<string>()))
                {
                    result.Add(new MinecraftVersion(version.Value<string>()));
                }
            }
            return result;
        }

        public async Task<IList<IMinecraftVersion>> GetMinecraftVersionsAsync()
        {
            List<IMinecraftVersion> result = new List<IMinecraftVersion>();
            string versions = await _grabber.GetJsonFromUrlAsync(_mcVersionUri);
            JContainer cont = (JContainer)JsonConvert.DeserializeObject(versions);

            foreach (var version in cont)
            {
                if (!String.IsNullOrWhiteSpace(version.Value<string>()))
                {
                    result.Add(new MinecraftVersion(version.Value<string>()));
                }
            }
            return result;
        }

        public IList<IMinecraftMod> SearchModByName(string name)
        {
            throw new NotImplementedException();
        }
        public Task<IList<IMinecraftMod>> SearchModByNameAsync(string name)
        {
            throw new NotImplementedException();
        }

        public IList<IMinecraftMod> GetAllModsByVersion(IMinecraftVersion version)
        {
            if(version == null)
                throw new ArgumentNullException("version");

            string mods = _grabber.GetJsonFromUrl(String.Format("{0}/{1}.json", _baseUrl, version.Version));
            if (!String.IsNullOrWhiteSpace(mods))
            {
                IList<IMinecraftMod> resultList = new List<IMinecraftMod>();
                JContainer jsonContainer = (JContainer) JsonConvert.DeserializeObject(mods);

                foreach (var cont in jsonContainer)
                {
                    var mod = JsonConvert.DeserializeObject<MinecraftMod>(cont.ToString());
                    resultList.Add(mod);
                }
                return resultList;
            }
            else
            {
                return new List<IMinecraftMod>();
            }
        }

        public async Task<IList<IMinecraftMod>> GetAllModsByVersionAsync(IMinecraftVersion version)
        {
            if (version == null)
                throw new ArgumentNullException("version");

            string mods = await _grabber.GetJsonFromUrlAsync(String.Format("{0}/{1}.json", _baseUrl, version.Version));
            if (!String.IsNullOrWhiteSpace(mods))
            {
                IList<IMinecraftMod> resultList = new List<IMinecraftMod>();
                JContainer jsonContainer = (JContainer)JsonConvert.DeserializeObject(mods);

                foreach (var cont in jsonContainer)
                {
                    var mod = JsonConvert.DeserializeObject<MinecraftMod>(cont.ToString());
                    resultList.Add(mod);
                }
                return resultList;
            }
            else
            {
                return new List<IMinecraftMod>();
            }
        }
    }
}
