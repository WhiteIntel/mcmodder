﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.Zip;
using McModder.CommonModels;
using McModder.Core;
using McModder.Interface;

namespace McModder.CoreAccess
{
    public class LocalAccessor : ILocalAccessor
    {
        private LocalGrabber _grabber;
        private const string MCMODINFO = "mcmod.info";

        public LocalAccessor(string modPath)
        {
            if (String.IsNullOrWhiteSpace(modPath))
                throw new ArgumentNullException("modPath");
            if (!Directory.Exists(modPath))
                throw new DirectoryNotFoundException(String.Format("The directory '{0}' does not exist!", modPath));

            _grabber = new LocalGrabber(modPath);
        }
        public IList<IMinecraftMod> GetAllMods()
        {
            IList<string> localModPaths = _grabber.GrabLocalMods();
            IList<IMinecraftMod> localMods = new List<IMinecraftMod>();

            foreach (var localMod in localModPaths)
            {
                ZipFile zf = new ZipFile(localMod);
                ZipEntry entry = zf.GetEntry(MCMODINFO);
                
                if (entry != null)
                {
                    Stream stream = zf.GetInputStream(entry);
                    string s = new StreamReader(stream).ReadToEnd();
                    try
                    {
                        MinecraftMod m = new MinecraftMod();
                        m.LocalModId = Regex.Match(s, "\\\"modid\\\"\\s*:\\s*\\\".+\\\"").Value.Split('\"')[3];
                        m.Name = Regex.Match(s, "\\\"name\\\"\\s*:\\s*\\\".+\\\"").Value.Split('\"')[3];
                        m.LocalVersion = Regex.Match(s, "\\\"version\\\"\\s*:\\s*\\\".+\\\"").Value.Split('\"')[3];
                        m.LocalPath = localMod;
                        localMods.Add(m);
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show("Fehler beim lesen:\r\n" + s);
                    }
                }
            }
            return localMods;
        }

        public async Task<IList<IMinecraftMod>> GetAllModsAsync()
        {
            IList<string> localModPaths = _grabber.GrabLocalMods();
            IList<IMinecraftMod> localMods = new List<IMinecraftMod>();

            foreach (var localMod in localModPaths)
            {
                ZipFile zf = new ZipFile(localMod);
                ZipEntry entry = zf.GetEntry(MCMODINFO);

                if (entry != null)
                {
                    Stream stream = zf.GetInputStream(entry);
                    string s = await new StreamReader(stream).ReadToEndAsync().ConfigureAwait(false);
                    try
                    {
                        MinecraftMod m = new MinecraftMod();
                        m.LocalModId = Regex.Match(s, "\\\"modid\\\"\\s*:\\s*\\\".+\\\"").Value.Split('\"')[3];
                        m.Name = Regex.Match(s, "\\\"name\\\"\\s*:\\s*\\\".+\\\"").Value.Split('\"')[3];
                        m.LocalVersion = Regex.Match(s, "\\\"version\\\"\\s*:\\s*\\\".+\\\"").Value.Split('\"')[3];
                        m.LocalPath = localMod;
                        localMods.Add(m);
                    }
                    catch (Exception e)
                    {
                        //MessageBox.Show("Fehler beim lesen:\r\n" + s);
                    }
                }
            }
            return localMods;
        }
    }
}
