﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonExtensions;
using DiffMatchPatch;
using McModder.Interface;

namespace McModder.Core
{
    public sealed class ModAnalyzer
    {
        public bool IsNearlyEqual(IMinecraftMod mod, IMinecraftMod secondMod)
        {
            List<Diff> diff = new List<Diff>();
            List<double> resultList = new List<double>();

            diff = mod.LocalModId.ToLower().Differences(secondMod.LocalModId.ToLower());
            var equalities = diff.Where(e => e.operation == Operation.EQUAL).ToList();
            resultList.Add(diff.CalcRelativeEqualityLevel());

            diff = mod.LocalModId.ToLower().Differences(secondMod.Name.ToLower());
            equalities = diff.Where(e => e.operation == Operation.EQUAL).ToList();
            resultList.Add(diff.CalcRelativeEqualityLevel());

            diff = mod.Name.ToLower().Differences(secondMod.Name.ToLower());
            equalities = diff.Where(e => e.operation == Operation.EQUAL).ToList();
            resultList.Add(diff.CalcRelativeEqualityLevel());

            diff = mod.LocalModId.ToLower().Differences(secondMod.LocalModId.ToLower());
            equalities = diff.Where(e => e.operation == Operation.EQUAL).ToList();
            resultList.Add(diff.CalcRelativeEqualityLevel());

            foreach (string alias in mod.Aliases)
            {
                diff = alias.ToLower().Differences(secondMod.Name.ToLower());
                equalities = diff.Where(e => e.operation == Operation.EQUAL).ToList();
                resultList.Add(diff.CalcRelativeEqualityLevel());

                diff = alias.ToLower().Differences(secondMod.LocalModId.ToLower());
                equalities = diff.Where(e => e.operation == Operation.EQUAL).ToList();
                resultList.Add(diff.CalcRelativeEqualityLevel());
            }

            foreach (string alias in secondMod.Aliases)
            {
                diff = alias.ToLower().Differences(mod.Name.ToLower());
                equalities = diff.Where(e => e.operation == Operation.EQUAL).ToList();
                resultList.Add(diff.CalcRelativeEqualityLevel());

                diff = alias.ToLower().Differences(mod.LocalModId.ToLower());
                equalities = diff.Where(e => e.operation == Operation.EQUAL).ToList();
                resultList.Add(diff.CalcRelativeEqualityLevel());
            }

            foreach (double result in resultList)
            {
                if (result > 75.0)
                    return true;
            }

            return false;
        }
    }
}
