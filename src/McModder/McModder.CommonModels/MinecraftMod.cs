﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using McModder.Interface;

namespace McModder.CommonModels
{
    public class MinecraftMod : IMinecraftMod
    {
        public MinecraftMod()
        {
            Name = "";
            Version = "";
            LongUrl = "";
            Comment = "";
            Author = "";
            LastUpdated = "";
            Dependencies = new List<string>();
            LocalVersion = "";
            LocalPath = "";
            LocalModId = "";
            Aliases = new List<string>();
        }

        public IList<string> Aliases { get; set; }
        public string Name { get; set; }

        public string Version { get; set; }

        public string LongUrl { get; set; }

        public string Comment { get; set; }

        public string Author { get; set; }

        public string LastUpdated { get; set; }

        public IList<string> Dependencies { get; set; }

        public string LocalVersion { get; set; }
        public string LocalPath { get; set; }
        public string LocalModId { get; set; }

        public override string ToString()
        {
            return String.Format("{0} // {1}", Name, LocalVersion);
        }

        public string Dev { get; set; }
    }
}
