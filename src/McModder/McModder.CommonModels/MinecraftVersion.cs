﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using McModder.Interface;

namespace McModder.CommonModels
{
    public class MinecraftVersion : IMinecraftVersion
    {
        public MinecraftVersion(string version)
        {
            if(String.IsNullOrWhiteSpace(version))
                throw new ArgumentException("version");
            Version = version;
        }
        public string Version { get; set; }

        public override string ToString()
        {
            return Version;
        }
    }
}
