﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McModder.Service
{
    public class Utils
    {   
        private Utils()
        {
        }

        /// <summary>
        /// Tries to get the default Minecraft path, otherwiese it will return a
        /// </summary>
        /// <returns></returns>
        public static string TryGetDefaultMcPath()
        {
            string mcPath = String.Format("{0}\\{1}",
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".minecraft\\mods");
            if (Directory.Exists(mcPath))
                return mcPath;
            else
                return "";
        }
    }
}
