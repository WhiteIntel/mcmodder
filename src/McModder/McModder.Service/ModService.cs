﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonExtensions;
using DiffMatchPatch;
using McModder.CommonExtensions;
using McModder.CommonModels;
using McModder.Core;
using McModder.CoreAccess;
using McModder.Interface;

namespace McModder.Service
{
    public class ModService : IModService
    {
        private readonly string _modPath;
        private IOnlineAccessor _onlineAccessor;
        private ILocalAccessor _localAccessor;
        public ModService(string modPath)
        {
            if (String.IsNullOrWhiteSpace(modPath))
                throw new ArgumentNullException("modPath");
            _modPath = modPath;
            _onlineAccessor = new NotEnoughModsAccessor();
            _localAccessor = new LocalAccessor(modPath);
        }
        public IList<IMinecraftMod> GetMergedList(IMinecraftVersion version)
        {
            if(version == null)
                throw new ArgumentNullException("version");
            IList<IMinecraftMod> onlineMods = _onlineAccessor.GetAllModsByVersion(version);
            IList<IMinecraftMod> localMods = _localAccessor.GetAllMods();
            IList<IMinecraftMod> mergedList = new List<IMinecraftMod>();

            Parallel.ForEach(localMods, (i) =>
            {
                i.LocalModId = i.LocalModId.PatchedTrim();
                i.Name = i.Name.PatchedTrim();

            });

            Parallel.ForEach(onlineMods, (i) =>
            {
                i.LocalModId = i.LocalModId.PatchedTrim();
                i.Name = i.Name.PatchedTrim();

            });

            Parallel.ForEach(localMods, (mod) =>
            {
                IMinecraftMod tmpMod = new MinecraftMod();
                IMinecraftMod searchedMod = null;
                if (!String.IsNullOrWhiteSpace(mod.LocalModId))
                {
                    searchedMod =
                        onlineMods.AsOptimizedParallel().FirstOrDefault(
                            e => e.Name.ToLower() == mod.LocalModId.ToLower() || e.Name.ToLower() == mod.Name.ToLower() || mod.Aliases.Contains(e.Name.ToLower()) || mod.Aliases.Contains(e.LocalModId.ToLower()));


                }
                else
                {
                    searchedMod = onlineMods.AsOptimizedParallel().FirstOrDefault(e => e.Name.ToLower() == mod.Name.ToLower());
                }

                if (searchedMod != null)
                {
                    tmpMod = searchedMod;
                    tmpMod.LocalVersion = mod.LocalVersion;
                    tmpMod.LocalPath = mod.LocalPath;
                    tmpMod.LocalModId = mod.LocalModId;
                    mergedList.Add(tmpMod);
                }
                else
                {
                    mod.Version = "Not available";
                    mergedList.Add(mod);
                }
            });
            return mergedList;

        }

        public IList<IMinecraftMod> GetAvailableUpdates(IMinecraftVersion version)
        {
            IList<IMinecraftMod> mods = GetMergedList(version).Where(e => !String.IsNullOrWhiteSpace(e.LocalVersion)).ToList();
            return mods;
        }

        public IList<IMinecraftMod> CheckForMinecraftUpdate(IMinecraftVersion version)
        {
            throw new NotImplementedException();
        }


        public IList<IMinecraftVersion> GetAvailableMinecraftVersions()
        {
            return _onlineAccessor.GetMinecraftVersions();
        }


        public async Task<IList<IMinecraftMod>> GetMergedListAsync(IMinecraftVersion version)
        {
            if (version == null)
                throw new ArgumentNullException("version");
            //IList<IMinecraftMod> onlineMods = await _onlineAccessor.GetAllModsByVersionAsync(version);
            IList<IMinecraftMod> onlineMods = _onlineAccessor.GetAllModsByVersion(version);
            IList<IMinecraftMod> localMods = await _localAccessor.GetAllModsAsync();
            IList<IMinecraftMod> mergedList = new List<IMinecraftMod>();
            ModAnalyzer analyzer = new ModAnalyzer();
            

            Parallel.ForEach(localMods, (i) =>
            {
                i.LocalModId = i.LocalModId.PatchedTrim();
                i.Name = i.Name.PatchedTrim();

            });

            Parallel.ForEach(onlineMods, (i) =>
            {
                i.LocalModId = i.LocalModId.PatchedTrim();
                i.Name = i.Name.PatchedTrim();

            });

            foreach(var mod in localMods)
            {
                IMinecraftMod tmpMod = new MinecraftMod();
                IMinecraftMod searchedMod = null;

                foreach (var onlineMod in onlineMods)
                {
                    if (analyzer.IsNearlyEqual(mod, onlineMod))
                    {
                        searchedMod = onlineMod;
                        break;
                    }
                }

                if (searchedMod != null)
                {
                    tmpMod = searchedMod;
                    tmpMod.LocalVersion = mod.LocalVersion;
                    tmpMod.LocalPath = mod.LocalPath;
                    tmpMod.LocalModId = mod.LocalModId;
                    mergedList.Add(tmpMod);
                }
                else
                {
                    mod.Version = "Not available";
                    mergedList.Add(mod);
                }
            }
            return mergedList;
        }

        public Task<IList<IMinecraftMod>> GetAvailableUpdatesAsync(IMinecraftVersion version)
        {
            throw new NotImplementedException();
        }

        public Task<IList<IMinecraftMod>> CheckForMinecraftUpdateAsync(IMinecraftVersion version)
        {
            throw new NotImplementedException();
        }

        public async Task<IList<IMinecraftVersion>> GetAvailableMinecraftVersionsAsync()
        {
            return await _onlineAccessor.GetMinecraftVersionsAsync();
        }
    }
}
