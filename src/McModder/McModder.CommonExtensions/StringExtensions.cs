﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McModder.CommonExtensions
{
    public static class StringExtensions
    {
        public static string PatchedTrim(this string str)
        {
            List<char> result = str.ToList();
            result.RemoveAll(c => c == ' ');
            str = new string(result.ToArray());
            return str;
        }
    }
}
