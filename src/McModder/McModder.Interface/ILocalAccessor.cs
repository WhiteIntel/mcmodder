﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McModder.Interface
{
    public interface ILocalAccessor
    {
        IList<IMinecraftMod> GetAllMods();
        Task<IList<IMinecraftMod>> GetAllModsAsync();
    }
}
