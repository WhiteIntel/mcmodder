﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McModder.Interface
{
    public interface IMinecraftVersion
    {
        string Version { get; set; }
    }
}
