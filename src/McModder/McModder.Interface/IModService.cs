﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McModder.Interface
{
    public interface IModService
    {
        IList<IMinecraftMod> GetMergedList(IMinecraftVersion version);
        IList<IMinecraftMod> GetAvailableUpdates(IMinecraftVersion version);
        IList<IMinecraftMod> CheckForMinecraftUpdate(IMinecraftVersion version);
        IList<IMinecraftVersion> GetAvailableMinecraftVersions();

        Task<IList<IMinecraftMod>> GetMergedListAsync(IMinecraftVersion version);
        Task<IList<IMinecraftMod>> GetAvailableUpdatesAsync(IMinecraftVersion version);
        Task<IList<IMinecraftMod>> CheckForMinecraftUpdateAsync(IMinecraftVersion version);
        Task<IList<IMinecraftVersion>> GetAvailableMinecraftVersionsAsync();
    }
}
