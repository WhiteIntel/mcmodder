﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McModder.Interface
{
    public interface IMinecraftMod
    {
        string Name { get; set; }
        string Version { get; set; }
        string LongUrl { get; set; }
        string Comment { get; set; }
        string Author { get; set; }
        string LastUpdated { get; set; }
        IList<string> Dependencies { get; set; }
        string LocalVersion { get; set; }
        string LocalPath { get; set; }
        string LocalModId { get; set; }
        IList<string> Aliases { get; set; }
        string Dev { get; set; }
    }
}
