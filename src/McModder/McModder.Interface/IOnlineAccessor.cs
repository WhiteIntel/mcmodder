﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace McModder.Interface
{
    public interface IOnlineAccessor
    {
        IList<IMinecraftVersion> GetMinecraftVersions();
        Task<IList<IMinecraftVersion>> GetMinecraftVersionsAsync();
        IList<IMinecraftMod> SearchModByName(string name);
        Task<IList<IMinecraftMod>> SearchModByNameAsync(string name);

        IList<IMinecraftMod> GetAllModsByVersion(IMinecraftVersion version);
        Task<IList<IMinecraftMod>> GetAllModsByVersionAsync(IMinecraftVersion version);

    }
}
